### Problem Statement 13

Namrata’s favorite shape is the square. She loves them! She also likes square numbers too! So she wants to write a program to find out whether a given shape of area N can be formed as the sum of two squares with both being integers of course. Can you help her?


**Input Format:**


The first line of input contains one integer c<=100. This is the number of test cases. Then c lines follow, each of them consisting of exactly one integer 0<=n<=10^12.


**Output Format: **

For each test case output Yes if it is possible to represent given number as a sum of two squares and No if it is not possible.

**Sample Input:**
```
10
1
2
7
14
49
9
17
76
2888
27
```

**Sample Output:**
```
Yes
Yes
No
No
Yes
Yes
Yes
No
Yes
No
```