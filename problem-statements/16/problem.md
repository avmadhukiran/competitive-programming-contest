### Problem Statement 16

An employer is suspicious of his employees! He thinks that there is a great likelihood that they are going to quit. He decides to fire all those employees who are within D “distance” (where distance is defined below) of an employee A who had quit recently. Call this number D the threshold distance.


**The distance function B<sub>d</sub>**


The distance function B<sub>d</sub> (P, Q) between two vectors P, Q of the same dimension is the number of positions in which the corresponding components are different. Essentially it’s similar to the Hamming Distance!


**Example:**


Given two vectors P = (1,2,3) and Q = (4, 1, 3), B<sub>d</sub>(P, Q) = 1 + 1 + 0 = 2 as the 1st and 2nd components of vector P are different from the 1st and 2nd components in vector Q.


Each employee in the company is described by a vector with C components. Here each component is an integer. Call this vector the characteristic vector.


**Input format**


The first line of input consists of the integer T. Then the first line of each test case consists of three integers E, C and D. Here E is the number of employees in the company, C is the number of components of the vector characterizing each employee, and D is the threshold distance as defined above. Then E + 1 lines follow with the input as follows:


E<sub>00</sub> E<sub>01</sub> … E<sub>0C</sub>

E<sub>10</sub> E<sub>11</sub> … E<sub>1C</sub>

…

E<sub>E0</sub> E<sub>E1</sub> … E<sub>EC</sub>



The characteristics vector of the employee A who quit is given by (E<sub>00</sub>, E<sub>01</sub>, … , E<sub>0C</sub>)



The characteristics vector of employee A<sub>i</sub> is given by (E<sub>i0</sub>, E<sub>i1</sub>, …, E<sub>iC</sub>) where i ranges from 1 to E.



Overall each test case is as follows:


E C D

E<sub>00</sub> E<sub>01</sub> … E<sub>0C</sub>

E<sub>10</sub> E<sub>11</sub> … E<sub>1C</sub>

…

E<sub>E0</sub> E<sub>E1</sub> … E<sub>EC</sub>


**Output format**


The output is an integer Y that is the number of employees the employer is going to fire. Essentially, the number of employees whose characteristics satisfy the equation:


B<sub>d</sub>(A,A<sub>i</sub>) <= D


**Constraints**

1<= T <= 10000

1 <= E <= 100000

1 <= C <= 10000

1 <= D <= ceil (C/50)

1 <= E<sub>ij</sub> <= 10000000000


**Sample Input**
```
1
5 3 1
1 2 3
4 1 2
5 1 6
1 5 9
3 2 7
7 1 3
```
**Sample Output**
```
3
```